public class Move {
	final double velocity, angleVelocity, timeV, timeRotate;
	
	public Move(double velocity, double angleVelocity, double timeV, double timeRotate) {
		this.velocity = velocity;
		this.angleVelocity = angleVelocity;
		this.timeV = timeV;
		this.timeRotate = timeRotate;
	}
}
