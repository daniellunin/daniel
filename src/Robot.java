import javax.management.InvalidAttributeValueException;

public class Robot {
	final double r;
	final Vector position, direction;
	final double v, angleVelocity;
	final private static double eps = 1e-7;
	
	public Robot(double r, double v, double w, Vector position, Vector direction) 
			throws IllegalArgumentException {
		this.position = position;
		this.direction = direction.ort();
		if (r < 0)
			throw new IllegalArgumentException();
		this.r = r;
		if (v < 0)
			throw new IllegalArgumentException();
		this.v = v;
		if (w < 0)
			throw new IllegalArgumentException();
		this.angleVelocity = w;
	}
	
	boolean eq(double x, double y) {
		return Math.abs(x - y) < eps;
	}
	
	boolean gr(double x, double y) {
		return x > y && !eq(x, y);
	}
	
	public Robot move(Move currentMove) 
			throws IllegalArgumentException {
		if (gr(Math.abs(currentMove.velocity), v))
			 throw new IllegalArgumentException();
		if (gr(Math.abs(currentMove.angleVelocity), angleVelocity))
			 throw new IllegalArgumentException();
		Vector newPosition = position.add(direction.mul(currentMove.velocity * currentMove.timeV));
		Vector newDirection = direction.rotate(currentMove.angleVelocity * currentMove.timeRotate);
		return new Robot(r, v, angleVelocity, newPosition, newDirection);
	}
	
	public Vector front() {
		return position.add(direction.mul(r));
	}
}
