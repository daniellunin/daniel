import java.util.Random;

public class InsideNoise implements InsideNoises{
    private Random random = new Random();


    @Override
    public Move move(Move move) {
        double noise = random.nextDouble();
        return new Move(move.velocity*noise,  move.angleVelocity*noise, move.timeV, move.timeRotate);
    }
}
