
public class SimpleAI implements AI {
	private static final double eps = 1e-7;
	
	private static boolean eq(double x, double y) {
		return Math.abs(x - y) < eps;
	}
	
	@Override
	public Move getMove(Robot robot, Vector target, double timeDelta) {
		Vector targetDirection = target.sub(robot.position);
		double alpha = robot.direction.getAngleTo(targetDirection);
		if (!eq(alpha, 0)) {
			double rotateTime = Math.abs(alpha / robot.angleVelocity);
			return new Move(0, Math.signum(alpha) * robot.angleVelocity, 0, Math.min(timeDelta, rotateTime));
		}
		double distance = target.sub(robot.front()).length();
		return new Move(robot.v, 0, Math.min(distance / robot.v, timeDelta), 0);
	}
}
