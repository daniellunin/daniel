public class BetterAI implements AI {
	private static final double eps = 1e-7;
	
	private static boolean eq(double x, double y) {
		return Math.abs(x - y) < eps;
	}
	
	private static boolean ls(double x, double y) {
		return x < y && !eq(x, y);
	}
	
	private static double countSimpleTime(Robot robot, Vector target) {
		Vector targetDirection = target.sub(robot.position);
		double alpha = robot.direction.getAngleTo(targetDirection);
		double dist = target.sub(robot.front()).length();
		return Math.abs(alpha) / robot.angleVelocity + dist / robot.v;
	}
	
	@Override
	public Move getMove(Robot robot, Vector target, double timeDelta) {
		Vector targetDirection = target.sub(robot.position);
		double alpha = robot.direction.getAngleTo(targetDirection);
		double curDist = target.sub(robot.front()).length();
		double curMaxTime = countSimpleTime(robot, target);
		double linearTime = Math.min(timeDelta, curDist / robot.v);
		if (!eq(alpha, 0)) {
			Vector newPosition = robot.position.add(robot.direction.mul(robot.v * timeDelta));
			Vector newDir = robot.direction.rotate(
					Math.signum(alpha) * Math.min(Math.abs(alpha), 
												  robot.angleVelocity * timeDelta));
			Robot newRobot = new Robot(robot.r, robot.v, robot.angleVelocity, newPosition, newDir);
			double newTime = countSimpleTime(newRobot, target) + timeDelta;
			double rotateTime = Math.min(timeDelta, Math.abs(alpha) / robot.angleVelocity);
			if (newTime < curMaxTime)
				return new Move(robot.v, Math.signum(alpha) * robot.angleVelocity, linearTime, Math.min(timeDelta, rotateTime));
			return new Move(0, Math.signum(alpha) * robot.angleVelocity, 0, Math.min(timeDelta, rotateTime));
		}
		return new Move(robot.v, 0, linearTime, 0);
	}
}
