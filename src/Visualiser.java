import javax.swing.*;
import java.awt.*;
import java.util.concurrent.TimeUnit;

public class Visualiser extends JFrame {
    private World world;

    public Visualiser(World world){
        super("ROBOT");
        this.world = world;
        setBounds(500, 60, 1000, 1000);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void drawCircle(Graphics g, Vector center, double r) {
        int intR = (int)r;
        int centerX = (int)center.x;
        int centerY = (int)center.y;
        g.drawOval(centerX - intR, centerY - intR, intR * 2, intR * 2);
    }

    private void  drawRobot(Graphics g, Robot robot) {
        Color colorNow = g.getColor();
        g.setColor(Color.BLACK);
        drawCircle(g, robot.position, robot.r);
        Vector nose = robot.direction.setLength(robot.r).add(robot.position);
        g.setColor(Color.BLUE);
        drawCircle(g, nose, robot.r / 4);
        g.setColor(colorNow);
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        drawRobot(g, world.robot);
        g.setColor(Color.RED);
        drawCircle(g, world.targetPosition, world.robot.r / 4);
        g.setColor(Color.GREEN);
        for (Noise noise : world.noises) {
            if (noise instanceof Hubble) {
                Hubble hubble = (Hubble) noise;
                if (!hubble.deleted) {
                    drawCircle(g, hubble.getPosition(), world.robot.r / 10);
                }
            }
        }
        g.setColor(Color.BLACK);
        /*
        double time = Math.round(simulatior.getTimeFromStart() * 100) / 100.0;
        g.setFont(new Font("Verdana", Font.PLAIN, 18));
        g.drawString("Time: " + Double.toString(time), getSize().width / 2 - 50, getSize().height - 50);
        if (simulatior.isEndSimulation()) {
            g.setFont(new Font("Verdana", Font.PLAIN, 25));
            g.drawString("Charging complete", getSize().width / 2 - 120, getSize().height / 2);
        }
        */
    }



    public void start() throws InterruptedException {
    	setVisible(true);
    	int counter = 0;
    	while (!world.robot.front().equals(world.targetPosition)) {
    		world.tick();
    		counter++;
    		if (counter % 1000 == 0) {
	    		TimeUnit.MILLISECONDS.sleep(10);
	            repaint();
    		}
    	}
    }
}