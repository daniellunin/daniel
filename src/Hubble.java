import java.util.ArrayList;
import java.util.HashSet;

public class Hubble implements Noise {
    private Vector position;
    private double rotateAngle;
	boolean deleted;
    
    public Hubble(Vector position, double rotateAngle){
    	this.position = position;
    	this.rotateAngle = rotateAngle;
		this.deleted = false;
    }
    
	public Vector getPosition(){
		return this.position;
	}
	@Override
	public Robot move(Robot robot, double t) {
			if ((this.isUnder(robot) && (!this.deleted))) {
				this.deleted = true;
				return new Robot(robot.r, robot.v, robot.angleVelocity,
						robot.position, robot.direction.rotate(this.rotateAngle));
			}
		return new Robot(robot.r, robot.v, robot.angleVelocity,
				robot.position, robot.direction);
	}

	public int getHashCode() {
		return position.getHashCode() + (int)rotateAngle;
	}
	
	private static boolean equal(double x, double y) {
		return Math.abs(x - y) < 1e-5;
	}
	
	public boolean equals(Hubble other) {
		return position.equals(other.position) && equal(rotateAngle, other.rotateAngle);
	}

	public boolean isUnder(Robot robot) {
		return robot.position.sub(position).length() < robot.r;
	}
}
