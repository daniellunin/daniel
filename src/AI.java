public interface AI {
	public Move getMove(Robot robot, Vector target, double timeDelta);
}
