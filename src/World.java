import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;


public class World {
	public Robot robot;
	public Vector targetPosition;
	public HashSet<Noise> noises;
	public HashSet<InsideNoise> insideNoises;
	public boolean simulationFinished = false;
	public double time, timeDelta;
	AI curAI;
	
	boolean eq(double x, double y) {
		return Math.abs(x - y) < 1e-3;
	}
	
	public World() {}
	
	/*
	private void readData() {
		Scanner input = new Scanner(System.in);
		HashMap<String, Double> data = new HashMap<String, Double>();
		System.out.print("R: ");
		data.put("radius", input.nextDouble());
		System.out.print("V W: ");
		robotVelocity = input.nextDouble();
		robotAngleVelocity = input.nextDouble();
		System.out.print("Start: ");
		startPosition = Vector.scan(input);
		System.out.print("Angle: ");
		startAngle = input.nextDouble();
		startDirection = new Vector(1, 0);
		startDirection = startDirection.rotate(startAngle);
		System.out.print("Target: ");
		targetPosition = Vector.scan(input);
	}
	*/
	
	private Robot move() {
		for (Noise noise : noises) {
		    robot = noise.move(robot, timeDelta);
		}
		Move curMove = curAI.getMove(robot, targetPosition, timeDelta);
		for (InsideNoise noise : insideNoises) {
			Move noisedMove = noise.move(curMove);
			robot = robot.move(noisedMove);
		}
		return robot;
	}
	
	public void run(Hashtable<String, Double> data, AI curAI) {
		//readData();
		double r = data.get("radius");
		Vector startPosition = new Vector(data.get("robotX"), data.get("robotY"));
		double startAngle = data.get("rotationAngle");
		Vector startDirection = new Vector(1, 0);
		startDirection = startDirection.rotate(startAngle);
		double robotVelocity = data.get("maxLinearSpeed");
		double robotAngleVelocity = data.get("maxAngularSpeed");
		robot = new Robot(r, robotVelocity, robotAngleVelocity, 
				startPosition, startDirection);
		targetPosition = new Vector(data.get("targetX"), data.get("targetY"));
		InsideNoise inNoise = new InsideNoise();
		insideNoises = new HashSet<>();
		insideNoises.add(inNoise);
		Wind wind = new Wind(new Vector(0.5, 0.5));
		noises = new HashSet<>();
		noises.add(new Hubble(new Vector(190, 190), 1));
		noises.add(new Hubble(new Vector(170, 170), 1));
		noises.add(new Hubble(new Vector(179, 179), 1));
		noises.add(wind);
		time = 0;
		timeDelta = 0.0001;
		if (!eq(robot.v, 0))
			timeDelta = Math.min(0.0001, 1e-3 / robot.v);
		this.curAI = curAI; 
		/*
		while (!robot.front().equalTo(targetPosition)) {
			tick();
		}
		simulationFinished = true;
		*/
		Visualiser v = new Visualiser(this);
		try {
			v.start();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("%.6f\n", time);
	}
	
	public void tick() {
		robot = move();
		time += timeDelta;
	}
}

/*
 7
V W: 25 0,5
Start: 57 42
Angle: 0
Target: 0 0

R: 7
V W: 10 1
Start: 58 67
Angle: 2
Target: 0 0
*/
