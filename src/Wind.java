import java.util.HashSet;

public class Wind implements Noise {
	private Vector velocity;
	
	Wind(Vector velocity) {
		this.velocity = velocity;
	}
	
	@Override
	public Robot move(Robot robot, double t) {
		return new Robot(robot.r,  robot.v, robot.angleVelocity, 
				robot.position.add(velocity.mul(t)), robot.direction);
	}
}
