import java.util.Hashtable;


public class Main {
	public static void main(String[] args) {
		World world = new World();
		AI simpleAI = new SimpleAI();
		AI betterAI = new BetterAI();
		Hashtable<String, Double> testConfig1 = new Hashtable<String,Double>(){{put("robotX",100.0);put("robotY",100.0);put("rotationAngle",0.0);put("maxLinearSpeed",10.0);put("maxAngularSpeed",0.2);put("radius",50.0); put("targetX",200.0);put("targetY",200.0);};};
		Hashtable<String, Double> testConfig2 = new Hashtable<String,Double>(){{put("robotX",500.0);put("robotY",100.0);put("rotationAngle",Math.PI);put("maxLinearSpeed",1.0);put("maxAngularSpeed",0.05);put("radius",5.0); put("targetX",600.0);put("targetY",200.0);};};
		Hashtable<String, Double> testConfig3 = new Hashtable<String,Double>(){{put("robotX",0.0);put("robotY",1.0);put("rotationAngle",2.0);put("maxLinearSpeed",10.0);put("maxAngularSpeed",1.0);put("radius",7.0); put("targetX",20.0);put("targetY",1.0);};};
		Hashtable<String, Double> testConfig4 = new Hashtable<String,Double>(){{put("robotX",200.0);put("robotY",700.0);put("rotationAngle",Math.PI / 2);put("maxLinearSpeed",50.0);put("maxAngularSpeed",0.01);put("radius",50.0); put("targetX",600.0);put("targetY",700.0);};};
		Hashtable<String, Double> testConfig5 = new Hashtable<String,Double>(){{put("robotX",0.0);put("robotY",0.0);put("rotationAngle",0.0);put("maxLinearSpeed",1.0);put("maxAngularSpeed",1.0);put("radius",1.0); put("targetX",0.0);put("targetY",1000.0);};};															
		Hashtable<String, Double> testConfig6 = new Hashtable<String,Double>(){{put("robotX",0.0);put("robotY",0.0);put("rotationAngle",0.0);put("maxLinearSpeed",10.0);put("maxAngularSpeed",0.1);put("radius",30.0); put("targetX",-100.0);put("targetY",-200.0);};};														
		Hashtable<String, Double> testConfig7 = new Hashtable<String,Double>(){{put("robotX",123.0);put("robotY",321.0);put("rotationAngle",Math.PI * 3 / 4);put("maxLinearSpeed",1.0);put("maxAngularSpeed",0.01);put("radius",100.0); put("targetX",120.0);put("targetY",321.0);};};															
		Hashtable<String, Double> testConfig8 = new Hashtable<String,Double>(){{put("robotX",100.0);put("robotY",200.0);put("rotationAngle",Math.PI / 2);put("maxLinearSpeed",10.0);put("maxAngularSpeed",0.01);put("radius",90.0); put("targetX",100.0);put("targetY",100.0);};};
		world.run(testConfig1, betterAI);
		world.run(testConfig1, simpleAI);
		System.out.println("---");
		world.run(testConfig2, simpleAI);
		world.run(testConfig2, betterAI);
		System.out.println("---");
		world.run(testConfig3, simpleAI);
		//world.run(testConfig3, betterAI);
		System.out.println("---");
		world.run(testConfig4, simpleAI);
		world.run(testConfig4, betterAI);
		System.out.println("---");
		world.run(testConfig5, simpleAI);
		world.run(testConfig5, betterAI);
		System.out.println("---");
		world.run(testConfig6, simpleAI);
		world.run(testConfig6, betterAI);
		System.out.println("---");
		//world.run(testConfig7, simpleAI);
		//world.run(testConfig7, betterAI);
		world.run(testConfig8, simpleAI);
		//world.run(testConfig8, betterAI);
	}
}
