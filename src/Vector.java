import java.util.Scanner;

public class Vector {
	public final double x, y;
	private final static double eps = 1e-3;
	
	public Vector() {
		x = 0;
		y = 0;
	}
	
	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector add(Vector other) {
		return new Vector(x + other.x, y + other.y);
	}
	
	public Vector sub(Vector other) {
		return new Vector(x - other.x, y - other.y);
	}
	
	public double cross(Vector other) {
		return x * other.y - other.x * y;
	}
	
	public double dot(Vector other) {
		return x * other.x + y * other.y;
	}
	
	public double length() {
		return Math.sqrt(x * x + y * y);
	}
	
	public Vector rotate(double alpha) {
		return new Vector(x * Math.cos(alpha) - y * Math.sin(alpha),
				x * Math.sin(alpha) + y * Math.cos(alpha));
	}
	
	public static Vector scan(Scanner input) {
		return new Vector(input.nextDouble(), input.nextDouble());
	}
	
	public Vector ort() {
		double len = length();
		return new Vector(x / len, y / len);
	}
	
	public Vector mul(double q) {
		return new Vector(x * q, y * q);
	}
	
	public Vector setLength(double len) {
		return ort().mul(len);
	}
	
	public boolean equals(Vector other) {
		return Math.abs(x - other.x) < eps && Math.abs(y - other.y) < eps;
	}
	
	public int getHashCode() {
		return (int)(x + y);
	}
	
	public double getAngleTo(Vector other) {
		return Math.atan2(cross(other), dot(other));
	}
}
