import java.util.HashSet;

public interface Noise {
	public Robot move(Robot robot, double t);
}
